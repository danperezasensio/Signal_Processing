clear,clc,clf, close all

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

w0 = 1600*pi;
f0 = w0/(2*pi);
Tc = 5*1e-6;
t = 0:Tc:0.2;
xc = cos(w0*t + pi/3);
fs = 5000;
Ts = 1/fs;
Np = 8;
figure;
subplot(211); plot(t,xc);
axis([0 Np/f0 -1 1]); grid on;

paso = fix(Ts/Tc);
xd = xc(1:paso:end);
n = 0:length(xd)-1;

subplot(212); stem(n,xd);
axis([0 (Np/f0)*fs -1 1]); grid on;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

N = 2000;
Xc = fftshift(fft(xc,N));
X = fftshift(fft(xd,N));
Omega = (-pi):(2*pi/N):(pi-2*pi/N);
omega = Omega/Tc;

figure;
subplot(211); plot(omega, abs(Xc));
xlabel '\omega (rad/s)'
ylabel '|X_c(j\omega)|'
axis([-fs*pi,fs*pi,0,1.2*max(abs(Xc))]);
subplot(212); plot(Omega, abs(X));
xlabel '\Omega (rad/s)'
ylabel '|X_d(j\Omega)|'

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

f2 = 3000;  % Frecuencia de muestreo
[L, M] = rat(f2/fs);    
y = resample(xd, L, M);
m = 0:length(y)-1;
Y = fftshift(fft(y,N));
figure;
subplot(211)
stem(m,y);
axis([0 (Np/f0)*f2 -1.1 1.1])
xlabel 'n'
ylabel 'x[n]'
subplot(212)
plot(Omega, abs(Y));
xlabel '\Omega (rad)'
ylabel 'X_c(j\Omega)'




