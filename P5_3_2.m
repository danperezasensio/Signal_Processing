close all;clear all;

%Frecuencias en tiempo discreto
Op=pi/4;
Os=pi/2;

%Período de muestreo
Ts=1024;

%Frecuencias en tiempo continuo (w=Ω/Ts)
Wp=(2*atan((Op)*1/2));
Ws=(2*atan((Os)*1/2));

Rp=20*log10(1)-20*log10(0.9);
Rs=20*log10(0.02);

%Nos devuelve el filtro digital de orden N menor que cumple con 
%las especificaciones.
%Wp y Ws tienen que ser escalares o 2-N-dimensiones.
%Rp y Rs tienen que ser escalares.
[N,Wn]=buttord(Wp,Ws,Rp,Rs,'s'); %La 's' es para filtro analógico.

%Calculamos el filtro de Butterworth digital paso bajo.
%Nos devuelve los coeficientes del filtro.
[B,A]=butter(N,Wn,'s');

%Calculamos mediante el método de la transformación invariante al impulso.
[bz,az]=bilinear(B,A,1);    %Fs=1Hz

%Obtención de la respuesta en frecuencia
[H, w]=freqz(bz,az,Ts);

%Representación de la respuesta en frecuencia
figure();hold on;

%%%Dibujamos la plantilla de especificaciones
xdata = [0                  0               pi/2;
         pi/4               pi/4            pi;
         pi/4               pi/4            pi;
         0                  0               pi/2];
     
ydata = [20*log10(0.0001)   20*log10(1)     20*log10(0.02);
         20*log10(0.0001)   20*log10(1)     20*log10(0.02);
         20*log10(0.9)      20*log10(4)     20*log10(4);
         20*log10(0.9)      20*log10(4)     20*log10(4)];

zdata = ones(4,3);
patch(xdata,ydata,zdata,'w');
title('Plantilla de especificaciones');
xlabel('Normalized frequency (rad)'); 
ylabel('Magnitude (dB)');

%%%Representamos la respuesta del filtro.
plot(w, 20*log10(abs(H)));
axis([0 pi 20*log10(0.0001) 20*log10(4) ]);
hold off;
