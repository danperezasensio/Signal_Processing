
clear all; close all;

%%% 3.1.1
L=50;
n=0:L-1;
x=cos(0.45*pi*n);
N=4000;
X=fft(x, N);
w=0:2*pi/N:2*pi*(1-1/N);
figure();
plot(w,abs(X)); xlabel('\Omega rad');ylabel('|X[\Omega]|');grid on;
axis([0 max(w) 0 ceil(max(abs(X)))]);

%%% 3.1.2
%M≥L donde M es el orden de la DFT; Omega=k*2pi/N donde k=0...N-1
%Omega=0.45pi -> 45*2pi/200=k*2pi/4000; k=45*4000/200=900;
%k=900; 900*2pi/4000=9*2pi/40; k=9; M=40; Como M≥L; M=2*M=80; k=2*k=18;
hold on;
M=80;
X=fft(x,M);
w=0:2*pi/M:2*pi*(1-1/M);
stem(w,abs(X));

%%% 3.1.3
L=50;
n=0:L-1;
x=cos(0.45*pi*n);
N=4096;
X=fft(x, N);
w=0:2*pi/N:2*pi*(1-1/N);
figure();
plot(w,abs(X)); xlabel('\Omega rad');ylabel('|X[\Omega]|');grid on;
axis([0 max(w) 0 ceil(max(abs(X)))]);
hold on;
M=80;
X=fft(x,M);
w=0:2*pi/M:2*pi*(1-1/M);
stem(w,abs(X));

%%% 3.1.4
M=80;
L=M;
n=0:L-1;
x=cos(0.45*pi*n);
N=4000;
X=fft(x, N);
w=0:2*pi/N:2*pi*(1-1/N);
figure();
plot(w,abs(X)); xlabel('\Omega rad');ylabel('|X[\Omega]|');grid on;
axis([0 max(w) 0 ceil(max(abs(X)))]);
hold on;
X=fft(x,M);
w=0:2*pi/M:2*pi*(1-1/M);
stem(w,abs(X));

%%% 3.2.1
%
L=200;
n=0:L-1;
x=cos(0.4*pi*n) + cos(2*pi*n/3);
N=4500;
w=0:2*pi/N:2*pi*(1-1/N);
M=





