% filtro FIR Kaiser 
Op=0.3*pi;
Oa=0.5*pi;
ds1=0.1;
ds2=0.01;

f = [Op Oa];
a = [1 0];
dev = [ds1 ds2];
[N,Wn,beta,ftype] = kaiserord(f,a,dev,2*pi);
Wn;
M=N/2;

L = 2*M+1;
n = -M:1:M;
Oc = 3*pi/8;
hi = (Oc/pi)*sinc(Oc*n/pi);

v = kaiser(L,beta)';
hd = hi.*v;

n1 = 0:1:L-1;
hf = hd;
figure;
subplot(311);
stem(n,hi);
title('Respuesta al impulso del filtro paso bajo ideal');
subplot(312);
stem(n,v);
title('Ventana de kaiser');
subplot(313);
stem(n1,hf);
title('Respuesta al impulso del filtro paso bajo diseñado');

xn=cos(0.2*pi*n)+cos(0.6*pi*n);
yn=conv(xn,hf);
n2=0:length(yn)-1;
figure()
stem(n2,yn);
title('salida Yn')

N=4096;
H=fft(hf,N);
Y=fft(yn,N);
w=(0:N-1)*2*pi/N;

figure;
plot(w , abs(H));
hold on
plot(w, abs(Y), 'r');
hold off
axis([0 2*pi 0 15]);


