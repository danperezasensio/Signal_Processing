
%%%%%%%%%%%%%%%%%%%%%%%%% DANIEL P�REZ ASENSIO %%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%              %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%    PRACTICA 5    %%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%              %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%    Ejercicio 3     %%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%               Diseño de filtros IIR                 %%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%     3.1     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear; close all;
N = 1024; % puntos para la fft

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DISE�O DE LA PLANTILLA DE ESPECIFICACIONES DE UN FILTRO PASO BAJO
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

xdata = [0                  0               pi/2;
         pi/4               pi/4            pi;
         pi/4               pi/4            pi;
         0                  0               pi/2];
     
ydata = [20*log10(0.0001)   20*log10(1)     20*log10(0.02);
         20*log10(0.0001)   20*log10(1)     20*log10(0.02);
         20*log10(0.9)      20*log10(4)     20*log10(4);
         20*log10(0.9)      20*log10(4)     20*log10(4)];
     
zdata = ones(4,3);

Op=pi/4;
Oa=pi/2;

Wp=Op/pi;                        % frecuencia de paso (analogico)
Ws=Oa/pi;                        % frecuencia de atenuacion (analogico)

Rp=20*log10(1)-20*log10(0.9);        % rizado en la banda de paso en dB
Rs=20*log10(0.02);                   % magnitud de la atenuacion en dB

[n,Wn]=buttord...
    (pi*Wp,pi*Ws,Rp,Rs, 's');        % obtencion del orden del filtro
[b,a] = butter(n,Wn, 's');           % construccion del filtro analogico
[bz,az] = impinvar(b,a,1);           % paso del filtro al dominio discreto
[H, w]=freqz(bz,az,N); 

% REPRESENTACION
figure();                            % se crea una nueva ventana
hold on;                             % se permite que se solapen figuras
patch(xdata,ydata,zdata,'w');        % se dibuja la plantilla
title('Plantilla de especificaciones');
xlabel('Normalized frequency (rad)'); 
ylabel('Magnitude (dB)');

plot(w, 20*log10(abs(H)));           % se dibuja la respuesta del filtro
axis([0 pi 20*log10(0) 20*log10(4) ]);
hold off; 














